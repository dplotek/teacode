import React, { useEffect, useState } from "react";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import Checkbox from "@mui/material/Checkbox";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Paper from "@mui/material/Paper";
import TextField from "@mui/material/TextField";

const App = () => {
	const API = `https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json`;

	const [usersData, setUsersData] = useState(null);
	const [filter, setFilter] = useState("");
	const [checkedUsers, setCheckedUsers] = useState("");
	const [showData, setShowData] = useState(null);

	useEffect(() => {
		const getData = async () => {
			await fetch(API)
				.then(response => response.json())
				.then(data => data.reduce((a, b) => a.concat(b), []))
				.then(data =>
					data.sort(function (a, b) {
						if (a.last_name.toLowerCase() < b.last_name.toLowerCase()) {
							return -1;
						}
						if (a.last_name.toLowerCase() > b.last_name.toLowerCase()) {
							return 1;
						}
						return 0;
					}),
				)
				.then(data => {
					setUsersData(data);
					setShowData(data);
				});
		};
		if (usersData === null) {
			getData();
		}
	}, [,]);

	const filteredUsers = value => {
		const foundData = usersData.filter(user => {
			return (
				user.first_name.toLowerCase().includes(value.toLowerCase()) ||
				user.last_name.toLowerCase().includes(value.toLowerCase())
			);
		});
		setShowData(foundData);
	};

	const [checked, setChecked] = useState([0]);

	const handleToggle = user => {
		const currentIndex = checked.indexOf(user.id);
		const newChecked = [...checked];

		if (currentIndex === -1) {
			newChecked.push(user.id);
			setCheckedUsers(prevState => [...prevState, user.id]);
		} else {
			newChecked.splice(currentIndex, 1);
			setCheckedUsers(checkedUsers.filter(value => value !== user.id));
		}
		setChecked(newChecked);
	};

	useEffect(() => {
		if (checkedUsers.length <= 0) {
			return;
		} else {
			console.log(checkedUsers);
		}
	}, [checkedUsers]);

	return (
		<div>
			<Paper
				sx={{
					display: "flex",
					margin: "50px auto",
					width: 500,
				}}
			>
				<List
					sx={{
						width: "100%",
						bgcolor: "#fafafa",
						padding: 0,
					}}
				>
					<Paper
						sx={{
							background:
								"linear-gradient(135deg, rgb(98, 186, 81), rgb(4, 148, 164))",
							padding: 1,
							display: "flex",
							justifyContent: "center",
						}}
					>
						<TextField
							id="outlined-basic"
							label="Search"
							variant="outlined"
							value={filter}
							onChange={e => {
								setFilter(e.target.value);
								filteredUsers(e.target.value);
							}}
						/>
					</Paper>
					{showData !== null
						? showData.map(user => {
								const { avatar, first_name, last_name, id } = user;
								const labelId = `checkbox-list-label-${id}`;
								return (
									<ListItem key={id} disablePadding>
										<ListItemButton
											role={undefined}
											onClick={() => handleToggle(user)}
											dense
										>
											<Card
												sx={{
													width: 48,
												}}
											>
												<CardMedia
													component="img"
													image={avatar}
													height="48px"
												/>
											</Card>
											<ListItemText
												id={labelId}
												primary={`${first_name} ${last_name}`}
												sx={{ marginLeft: 10 }}
											/>
											<ListItemIcon>
												<Checkbox
													edge="start"
													checked={checked.indexOf(id) !== -1}
													disableRipple
												/>
											</ListItemIcon>
										</ListItemButton>
									</ListItem>
								);
						  })
						: ""}
				</List>
			</Paper>
		</div>
	);
};

export default App;
